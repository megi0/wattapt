'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">watta-pt documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AdminModule.html" data-type="entity-link">AdminModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AdminModule-daa9825db8fb855ca0773e9500d2b784"' : 'data-target="#xs-components-links-module-AdminModule-daa9825db8fb855ca0773e9500d2b784"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AdminModule-daa9825db8fb855ca0773e9500d2b784"' :
                                            'id="xs-components-links-module-AdminModule-daa9825db8fb855ca0773e9500d2b784"' }>
                                            <li class="link">
                                                <a href="components/AddAdminComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddAdminComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DeleteUserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DeleteUserComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' : 'data-target="#xs-components-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' :
                                            'id="xs-components-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageNotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PageNotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' : 'data-target="#xs-injectables-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' :
                                        'id="xs-injectables-links-module-AppModule-5b95e975d5678697ca2b9c88a8c01998"' }>
                                        <li class="link">
                                            <a href="injectables/BooksService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>BooksService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LibraryService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LibraryService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/BookModule.html" data-type="entity-link">BookModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-BookModule-11605b69624a146b41a382d14a241edf"' : 'data-target="#xs-components-links-module-BookModule-11605b69624a146b41a382d14a241edf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-BookModule-11605b69624a146b41a382d14a241edf"' :
                                            'id="xs-components-links-module-BookModule-11605b69624a146b41a382d14a241edf"' }>
                                            <li class="link">
                                                <a href="components/BookInnerViewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BookInnerViewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BookListViewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BookListViewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BookOuterViewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BookOuterViewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BookPresentationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BookPresentationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChapterListerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChapterListerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChapterToWriteComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChapterToWriteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CreateComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CreateNewBookComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateNewBookComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditBookComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EditBookComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/YourStoriesViewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">YourStoriesViewComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LibraryModule.html" data-type="entity-link">LibraryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LibraryModule-871288d0dd4eece2b02560f3aef0314b"' : 'data-target="#xs-components-links-module-LibraryModule-871288d0dd4eece2b02560f3aef0314b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LibraryModule-871288d0dd4eece2b02560f3aef0314b"' :
                                            'id="xs-components-links-module-LibraryModule-871288d0dd4eece2b02560f3aef0314b"' }>
                                            <li class="link">
                                                <a href="components/LibraryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LibraryComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginAndSignupModule.html" data-type="entity-link">LoginAndSignupModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginAndSignupModule-6cdcd9d52888d23d6495cbbac0dda87a"' : 'data-target="#xs-components-links-module-LoginAndSignupModule-6cdcd9d52888d23d6495cbbac0dda87a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginAndSignupModule-6cdcd9d52888d23d6495cbbac0dda87a"' :
                                            'id="xs-components-links-module-LoginAndSignupModule-6cdcd9d52888d23d6495cbbac0dda87a"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SignupComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalsModule.html" data-type="entity-link">ModalsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ModalsModule-3c9ef0970b715d8738fb857885eabf74"' : 'data-target="#xs-components-links-module-ModalsModule-3c9ef0970b715d8738fb857885eabf74"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalsModule-3c9ef0970b715d8738fb857885eabf74"' :
                                            'id="xs-components-links-module-ModalsModule-3c9ef0970b715d8738fb857885eabf74"' }>
                                            <li class="link">
                                                <a href="components/RatingModalComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RatingModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TaskCompletedModalComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TaskCompletedModalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UploaderModule.html" data-type="entity-link">UploaderModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UploaderModule-39d7d5d17949106331aaa33374ea1727"' : 'data-target="#xs-components-links-module-UploaderModule-39d7d5d17949106331aaa33374ea1727"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UploaderModule-39d7d5d17949106331aaa33374ea1727"' :
                                            'id="xs-components-links-module-UploaderModule-39d7d5d17949106331aaa33374ea1727"' }>
                                            <li class="link">
                                                <a href="components/FileUploaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FileUploaderComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AgeValidator.html" data-type="entity-link">AgeValidator</a>
                            </li>
                            <li class="link">
                                <a href="classes/FileType.html" data-type="entity-link">FileType</a>
                            </li>
                            <li class="link">
                                <a href="classes/RegexpValidator.html" data-type="entity-link">RegexpValidator</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthenticationService.html" data-type="entity-link">AuthenticationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BooksService.html" data-type="entity-link">BooksService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LibraryService.html" data-type="entity-link">LibraryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RatingService.html" data-type="entity-link">RatingService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StorageService.html" data-type="entity-link">StorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link">UsersService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UtilService.html" data-type="entity-link">UtilService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthenticationGuard.html" data-type="entity-link">AuthenticationGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/ConfirmLeaveGuard.html" data-type="entity-link">ConfirmLeaveGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Book.html" data-type="entity-link">Book</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ComponentCanDeactivate.html" data-type="entity-link">ComponentCanDeactivate</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/File.html" data-type="entity-link">File</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FilesUploadMetadata.html" data-type="entity-link">FilesUploadMetadata</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link">User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});