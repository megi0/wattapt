export interface Book {
  id?: string;
  title: string;
  genre?: string;
  cover: string;
  description?: string;
  author?: string;
  key?: string;
}
