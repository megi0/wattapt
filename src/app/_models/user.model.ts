export interface User {
  library?: any;
  uid: string;
  username: string;
  email: string;
  password: string;
  birthdate?: Date;
  role: string;
  id?: string;
}
