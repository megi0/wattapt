import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { User } from 'src/app/_models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  loggedIn: boolean = false;
  admin: boolean = false;

  isLoggedIn(): boolean {
    if (localStorage.getItem('email')) {
      return (this.loggedIn = true);
    }
    return false;
  }

  setLoggedIn(toLogInOrNotToLogIn: boolean) {
    this.loggedIn = toLogInOrNotToLogIn;
  }

  isAdmin(): boolean {
    if (localStorage.getItem('role') === 'admin') {
      return true;
    }
    return false;
  }

  setAdmin(admin: boolean) {
    localStorage.setItem('role', 'admin');
  }

  getAccountInfo(email: string, password: string): Observable<User[]> {
    return this.fs
      .collection('users', (ref) =>
        ref.where('email', '==', email).where('password', '==', password)
      )
      .valueChanges() as Observable<User[]>;
  }

  getAccountWithGivenEmail(email: string): Observable<User[]> {
    return this.fs
      .collection('users', (ref) => ref.where('email', '==', email))
      .valueChanges() as Observable<User[]>;
  }

  getAccountWithGivenUsername(username: string): Observable<User[]> {
    return this.fs
      .collection('users', (ref) => ref.where('username', '==', username))
      .valueChanges() as Observable<User[]>;
  }

  constructor(private fs: AngularFirestore) {}
}
