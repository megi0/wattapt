import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { Observable } from 'rxjs';
import { User } from 'src/app/_models/user.model';
import { Book } from '../_models/book.model';
import { finalize, map, switchMap, take } from 'rxjs/operators';
import { createSolutionBuilderWithWatchHost } from 'typescript';

@Injectable({
  providedIn: 'root',
})
export class BooksService {
  constructor(private fs: AngularFirestore) {}

  getAllBooks() {
    let snapshotarr: any[] = [];
    this.fs
      .collection('books', (ref) => ref.limit(10))
      .get()
      .subscribe((snapshot) => {
        snapshot.forEach((el) => {
          var item: any = el.data();
          item.key = el.id;
          snapshotarr.push(item);
        });
      });
    return snapshotarr;
  }

  getAllBooks1() {
    return this.fs
      .collection('books', (ref) => ref.limit(20))
      .valueChanges({ idField: 'key' });
  }

  getBookById(bookId: string | any) {
    return this.fs.collection('books').doc(bookId).valueChanges() as Observable<
      Book[]
    >;
  }

  getBookChapters(bookId: string | any): Observable<any> {
    return this.fs
      .collection('chapters', (ref) => ref.where('bookId', '==', bookId))
      .valueChanges() as Observable<any>;
  }

  getBookByProperty(property: string, propertyNameInDb: string) {
    return this.fs
      .collection('books', (ref) => ref.where(propertyNameInDb, '==', property))
      .valueChanges({ idField: 'key' }) as Observable<any>;
  }

  addChapterToNewBook(bookId: string) {
    this.fs.collection('chapters').add({
      bookId: bookId,
      chapters: [
        {
          title: '',
          content: '',
        },
      ],
    });
  }

  addChapterToExistingBook(bookId: string) {
    this.fs
      .collection('chapters', (ref) => ref.where('bookId', '==', bookId))
      .valueChanges({ idField: 'documentId' })
      .pipe(
        take(1),
        map((value) =>
          this.fs
            .collection('chapters')
            .doc(value[0].documentId)
            .update({
              chapters: firebase.firestore.FieldValue.arrayUnion({
                title: '',
                content: '',
              }),
            })
        )
      )
      .subscribe();
  }

  getChapterDocumentId(bookId: string) {
    let chapterId: string;
  }

  updateBook(bookId: string, bookContent: object) {
    this.fs
      .collection('chapters', (ref) => ref.where('bookId', '==', bookId))
      .valueChanges({ idField: 'documentId' })
      .pipe(
        take(1),
        map((value) =>
          this.fs
            .collection('chapters')
            .doc(value[0].documentId)
            .set(bookContent)
        )
      )
      .subscribe();
  }

  deleteBook(bookId: string) {
    this.fs.collection('books').doc(bookId).delete();
  }
}
