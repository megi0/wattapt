import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UsersService } from './users.service';
@Injectable({
  providedIn: 'root',
})
export class LibraryService {
  userId: string | any;
  uploadInProcess: Observable<any> = new Observable();
  constructor(private fs: AngularFirestore, private usersS: UsersService) {}

  addBookToLibrary(bookId: string, userEmail: string | any) {
    this.usersS
      .getIdByEmail(userEmail)
      .pipe(
        map((value: any) => {
          value.forEach((element: { id: string | undefined }) => {
            this.fs
              .collection('users')
              .doc(element.id)
              .update({
                library: firebase.firestore.FieldValue.arrayUnion(bookId),
              });
          });
          return true;
        })
      )
      .subscribe((value: any) => console.log(value));
  }

  getBooksFromLibrary(userEmail: string | any): Observable<any> {
    return this.fs
      .collection('users', (ref) => ref.where('email', '==', userEmail))
      .valueChanges() as Observable<any>;
  }

  removeBookFromLibrary(bookId: string, userEmail: string | any) {
    this.usersS
      .getIdByEmail(userEmail)
      .pipe(
        map((value: any) => {
          value.forEach((element: { id: string | undefined }) => {
            console.log(bookId);
            this.fs
              .collection('users')
              .doc(element.id)
              .update({
                library: firebase.firestore.FieldValue.arrayRemove(bookId),
              });
          });
        })
      )
      .subscribe();
  }
}
