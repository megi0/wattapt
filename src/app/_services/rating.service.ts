import { getRtlScrollAxisType } from '@angular/cdk/platform';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { filter, finalize, map, switchMap, take } from 'rxjs/operators';
import { BooksService } from './books.service';

@Injectable({
  providedIn: 'root',
})
export class RatingService {
  ratingProperties:
    | {
        bookId: number;
        currentRaters: number;
        currentRating: number;
        raters: [];
      }
    | any;
  constructor(private bookS: BooksService, private fireS: AngularFirestore) {}

  rate(bookId: string, userEmail: string, rating: number) {
    console.log('one', bookId);
    return this.getRatingDocumentByBookId(bookId)
      .pipe(
        take(1),
        map((value) => {
          console.log('two', value),
            this.fireS
              .collection('ratings')
              .doc(value[0].docId)
              .valueChanges()
              .pipe(
                take(1),
                map((element) => {
                  this.ratingProperties = element;
                  console.log('ratingProps: ', this.ratingProperties);
                  this.fireS
                    .collection('ratings')
                    .doc(value[0].docId)
                    .update({
                      bookId: this.ratingProperties.bookId,
                      currentRating: this.calculateTotalRating(
                        this.ratingProperties.currentRaters,
                        this.ratingProperties.currentRating,
                        rating
                      ),
                      currentRaters: this.ratingProperties.currentRaters + 1,
                      raters: [userEmail, ...this.ratingProperties.raters],
                    });
                })
              )
              .subscribe();
        })
      )
      .subscribe();
  }

  getRatingDocumentByBookId(bookId: string) {
    return this.fireS
      .collection('ratings', (ref) => ref.where('bookId', '==', bookId))
      .valueChanges({ idField: 'docId' });
  }

  checkIfUserHasRatedBook(bookId: string, userEamil: string): Observable<any> {
    return this.getRatingDocumentByBookId(bookId).pipe(
      switchMap(async (value) =>
        this.fireS
          .collection('ratings')
          .doc(value[0].docId)
          .valueChanges()
          .pipe(filter((rating: any) => rating.raters.includes(userEamil)))
          .subscribe()
      )
    );
  }

  calculateTotalRating(
    ratersNumber: number,
    currRating: number,
    addedRating: number
  ): number {
    return (
      (currRating * ratersNumber + addedRating) /
      (ratersNumber + 1)
    ).toFixed(2) as unknown as number;
  }

  createRatingEntry(bookId: string) {
    this.fireS.collection('ratings').doc().set({
      bookId: bookId,
      currentRaters: 0,
      currentRating: 0,
      raters: [],
    });
  }
}
