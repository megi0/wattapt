import { analyzeAndValidateNgModules } from '@angular/compiler';
import { utf8Encode } from '@angular/compiler/src/util';
import { Injectable, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { observable, Observable, of } from 'rxjs';
import { User } from '../_models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  ids: User[] | any;
  constructor(private fs: AngularFirestore) {}

  getAllUsers(): Observable<User[]> {
    return this.fs
      .collection('users')
      .valueChanges({ idField: 'id' }) as Observable<User[]>;
  }

  getCertainUser(email: string): Observable<User[]> {
    return this.fs
      .collection('users', (ref) => ref.where('email', '==', email))
      .valueChanges() as Observable<User[]>;
  }

  //get rid of this
  getIDs() {
    console.log('hello');
    this.fs
      .collection('users')
      .get()
      .subscribe((snapshot) => {
        snapshot.forEach((doc) => {
          console.log(doc.id, '=>', doc.data());
        });
      });
  }

  getIdByEmail(email: string | any): Observable<any> | any {
    console.log('haha');
    let documentId: Observable<any>;
    return this.fs
      .collection('users', (ref) => ref.where('email', '==', email))
      .get() as Observable<any>;
  }

  deleteUser(userId: string) {
    this.fs.collection('users').doc(userId).delete();
  }

  makeUserAdmin(userId: string) {
    this.fs.collection('users').doc(userId).update({ role: 'admin' });
  }
}
