import { ValidatorFn, AbstractControl } from '@angular/forms';

export class AgeValidator {
  static currentDate = new Date();

  static currentDateReq = (
    AgeValidator.currentDate.getDate() +
    '-' +
    AgeValidator.currentDate.getMonth() +
    '-' +
    AgeValidator.currentDate.getFullYear()
  ).toString();

  static propriateAgeValidator(control: AbstractControl): any | null {
    return AgeValidator.dateChecker(AgeValidator.currentDate)(control);
  }

  static dateChecker(date: Date): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (date.getFullYear() - control.value.substring(0, 4) > 13) {
        return null;
      } else {
        if (
          date.getFullYear() - control.value.substring(0, 4) == 13 &&
          date.getMonth() - (control.value - 1) >= 0
        )
          return null;
        else {
          return { notOldEnough: control.value };
        }
      }
    };
  }
}
