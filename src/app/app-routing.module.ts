import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeleteUserComponent } from './components/admin/delete-user/delete-user.component';
import { BookInnerViewComponent } from './components/books/book-inner-view/book-inner-view.component';
import { BookPresentationComponent } from './components/books/book-presentation/book-presentation.component';
import { ChapterToWriteComponent } from './components/books/chapter-to-write/chapter-to-write.component';
import { CreateNewBookComponent } from './components/books/create-new-book/create-new-book.component';
import { YourStoriesViewComponent } from './components/books/your-stories-view/your-stories-view.component';
import { LoginComponent } from './components/login-and-signup/login/login.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SignupComponent } from './components/login-and-signup/signup/signup.component';
import { HomeComponent } from './components/home/home.component';
import { LibraryComponent } from './components/library/library/library.component';
import { AuthenticationGuard } from './_guards/authentication.guard';
import { AddAdminComponent } from './components/admin/add-admin/add-admin.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthenticationGuard],
  },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  {
    path: 'new-book',
    component: CreateNewBookComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'library',
    component: LibraryComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'book/:id',
    component: BookPresentationComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'book/:id/read',
    component: BookInnerViewComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'write/:id',
    component: ChapterToWriteComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'my-stories',
    component: YourStoriesViewComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'admin',
    component: DeleteUserComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'admin-add',
    component: AddAdminComponent,
    canActivate: [AuthenticationGuard],
  },
  { path: '', component: HomeComponent, canActivate: [AuthenticationGuard] },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
