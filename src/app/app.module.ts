import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/nav/nav/nav.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { UsersService } from './_services/users.service';
import {
  AngularFireStorage,
  AngularFireStorageModule,
  AngularFireStorageReference,
} from '@angular/fire/storage';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { BookModule } from './components/books/book.module';
import { UploaderModule } from './components/file-uploader/uploader.module';
import { TaskCompletedModalComponent } from './components/modals/task-completed-modal/task-completed-modal.component';
import { ModalsModule } from './components/modals/modals.module';
import { LibraryService } from './_services/library.service';
import { BooksService } from './_services/books.service';
import { LibraryModule } from './components/library/library.module';
import { DeleteUserComponent } from './components/admin/delete-user/delete-user.component';
import { AdminModule } from './components/admin/admin.module';
import { LoginAndSignupModule } from './components/login-and-signup/login-and-signup.module';
import { RatingModule } from 'ng-starrating';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { AuthenticationGuard } from './_guards/authentication.guard';
import { AuthenticationService } from './_services/authentication.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NavModule } from './components/nav/nav.module';

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent, HomeComponent],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    BrowserAnimationsModule,
    FormsModule,
    BookModule,
    UploaderModule,
    ModalsModule,
    LibraryModule,
    AdminModule,
    LoginAndSignupModule,
    RatingModule,
    SlickCarouselModule,
    NgxUsefulSwiperModule,
    Ng2SearchPipeModule,
    NavModule,
  ],
  providers: [
    UsersService,
    LibraryService,
    BooksService,
    AuthenticationGuard,
    AuthenticationService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
