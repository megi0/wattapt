import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { AddAdminComponent } from './add-admin/add-admin.component';
import { LoginAndSignupModule } from '../login-and-signup/login-and-signup.module';

@NgModule({
  declarations: [DeleteUserComponent, AddAdminComponent],
  imports: [CommonModule, LoginAndSignupModule],
  exports: [DeleteUserComponent],
})
export class AdminModule {}
