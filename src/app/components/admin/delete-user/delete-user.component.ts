import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { UsersService } from 'src/app/_services/users.service';
import { User } from 'src/app/_models/user.model';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css'],
})
export class DeleteUserComponent implements OnInit {
  users: User[] | any;

  constructor(private userS: UsersService, private router: Router) {}

  ngOnInit(): void {
    this.userS
      .getAllUsers()
      .pipe(
        map((value) => {
          this.users = value.filter(
            (element) => element.email !== localStorage.getItem('email')
          );
          console.log(this.users);
        })
      )
      .subscribe();
  }

  deleteUser(userId: string) {
    this.userS.deleteUser(userId);
  }

  makeAdmin(userId: string) {
    this.userS.makeUserAdmin(userId);
  }

  addNewAdmin() {
    this.router.navigate(['/admin-add']);
  }
}
