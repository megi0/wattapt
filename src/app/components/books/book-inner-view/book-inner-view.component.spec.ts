import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookInnerViewComponent } from './book-inner-view.component';

describe('BookInnerViewComponent', () => {
  let component: BookInnerViewComponent;
  let fixture: ComponentFixture<BookInnerViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookInnerViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookInnerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
