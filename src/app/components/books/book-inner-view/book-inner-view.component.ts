import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { BooksService } from 'src/app/_services/books.service';

@Component({
  selector: 'app-book-inner-view',
  templateUrl: './book-inner-view.component.html',
  styleUrls: ['./book-inner-view.component.css'],
})
export class BookInnerViewComponent implements OnInit {
  chapterContainer: any;
  currentChapterContent: any;
  currentChapterTitle: any;
  emptyBook = false;
  constructor(private bookS: BooksService, private ar: ActivatedRoute) {}

  ngOnInit(): void {
    this.ar.paramMap
      .pipe(
        map((el) => {
          this.bookS
            .getBookChapters(el.get('id'))
            .pipe(
              map((val) => {
                if (val.length == 1 && val[0].chapters[0].content === '') {
                  this.emptyBook = true;
                }
                this.chapterContainer = val;
                this.currentChapterContent =
                  this.chapterContainer[0].chapters[0].content;
                this.currentChapterTitle =
                  this.chapterContainer[0].chapters[0].title;
              })
            )
            .subscribe();
        })
      )
      .subscribe();
  }

  changeChapter(chapterNo: number) {
    this.currentChapterContent =
      this.chapterContainer[0].chapters[chapterNo].content;
    this.currentChapterTitle =
      this.chapterContainer[0].chapters[chapterNo].title;
  }
}
