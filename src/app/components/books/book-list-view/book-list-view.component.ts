import { concat, forkJoin, Observable, Subject } from 'rxjs';
import {
  Component,
  ComponentFactoryResolver,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { BooksService } from 'src/app/_services/books.service';
import { LibraryService } from 'src/app/_services/library.service';
import { Book } from '../../../_models/book.model';
import { map, take } from 'rxjs/operators';
import { SwiperOptions, Mousewheel } from 'swiper';
import SwiperCore from 'swiper/core';
import { fadeInItems } from '@angular/material/menu';
SwiperCore.use([Mousewheel]);
@Component({
  selector: 'app-book-list-view',
  templateUrl: './book-list-view.component.html',
  styleUrls: ['./book-list-view.component.css'],
})
export class BookListViewComponent implements OnInit {
  booksToBeDisplayed: Book[] | any;
  booksToBeDisplayed1: any[] | any;
  booksToBeDisplayed2: any[] = [];
  libraryBooks: any[] | any;
  allBooks: any[] | any;
  coverUrl: Observable<string | null> | any = '';
  @ViewChild('whooosh') swiper: ElementRef | any;

  constructor(
    private bs: BooksService,
    private storage: AngularFireStorage,
    private libraryS: LibraryService
  ) {}

  onClick() {
    this.booksToBeDisplayed.forEach((element: { data: { title: any } }) => {
      console.log(element.data.title);
    });
  }

  ngOnInit(): void {
    this.libraryS
      .getBooksFromLibrary(localStorage.getItem('email'))
      .pipe(
        take(1),
        map((value) => {
          this.libraryBooks = value[0].library;
          console.log('libBooks siper');
          this.bs
            .getAllBooks1()
            .pipe(
              take(1),
              map((element) => {
                this.allBooks = element;
                console.log('nooooo');
                this.booksToBeDisplayed2 = this.booksNotInLibrary(
                  this.libraryBooks,
                  this.allBooks
                );
              })
            )
            .subscribe();
        })
      )
      .subscribe();
  }

  booksNotInLibrary(libBooks: [], allBooks: []) {
    let allowedBooks: any[] = [];
    allBooks.forEach((el: any) => {
      if (!libBooks.includes(el.key as never)) {
        allowedBooks = [...allowedBooks, el];
      }
    });
    console.log('HEY');
    return allowedBooks;
  }

  config: SwiperOptions = {
    loop: true,
    autoHeight: true,
    allowTouchMove: true,
    spaceBetween: 20,
    autoplay: false,
    /*{
      delay: 3000,
      disableOnInteraction: true,
    }*/ navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  };
}
