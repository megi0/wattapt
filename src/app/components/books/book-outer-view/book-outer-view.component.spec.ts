import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOuterViewComponent } from './book-outer-view.component';

describe('BookOuterViewComponent', () => {
  let component: BookOuterViewComponent;
  let fixture: ComponentFixture<BookOuterViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookOuterViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOuterViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
