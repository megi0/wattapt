import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { BooksService } from 'src/app/_services/books.service';
import { LibraryService } from 'src/app/_services/library.service';
import { Book } from 'src/app/_models/book.model';

@Component({
  selector: 'app-book-outer-view',
  templateUrl: './book-outer-view.component.html',
  styleUrls: ['./book-outer-view.component.css'],
})
export class BookOuterViewComponent implements OnInit {
  @Input() carouselBook: Book | any;
  @Input() canWrite: boolean = false;
  @Input() canAddToLibrary: boolean = false;
  @Input() canRemoveFromLibrary: boolean = false;
  displayModal: Subject<any> = new Subject();
  deleted: boolean = false;
  rate: string = '5';
  notRated = true;

  constructor(
    private bookS: BooksService,
    private router: Router,
    private libraryS: LibraryService
  ) {}

  ngOnInit(): void {}

  addToLibrary(bookId: string) {
    this.libraryS.addBookToLibrary(bookId, localStorage.getItem('email'));
    this.canAddToLibrary = false;
    this.canRemoveFromLibrary = true;
  }

  openFullBook(bookId: string) {
    this.router.navigate(['/book/', bookId]);
  }

  write(bookId: string) {
    this.router.navigate(['/write', bookId]);
  }

  removeFromLibrary(bookId: string) {
    this.libraryS.removeBookFromLibrary(bookId, localStorage.getItem('email'));
    this.canRemoveFromLibrary = false;
    this.canAddToLibrary = true;
    if (this.router.url !== '/') {
      this.deleted = true;
    }
  }

  openConfirmationModal() {
    this.displayModal.next(true);
  }

  delete(bookId: string) {
    this.bookS.deleteBook(bookId);
  }
}
