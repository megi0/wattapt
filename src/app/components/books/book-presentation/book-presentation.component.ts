import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, find, map, switchMap } from 'rxjs/operators';
import { BooksService } from 'src/app/_services/books.service';
import { LibraryService } from 'src/app/_services/library.service';
import { RatingService } from 'src/app/_services/rating.service';
import { Book } from 'src/app/_models/book.model';
import { UsersService } from 'src/app/_services/users.service';

@Component({
  selector: 'app-book-presentation',
  templateUrl: './book-presentation.component.html',
  styleUrls: ['./book-presentation.component.css'],
})
export class BookPresentationComponent implements OnInit {
  bookId: string | any = '';
  bookInfoContainer: Book | any;
  displayModal: Subject<any> = new Subject();
  notRated: boolean = true;
  rating: number | any;
  bookAuthor: boolean = false;
  inLibrary: boolean = false;
  constructor(
    private ar: ActivatedRoute,
    private bookS: BooksService,
    private router: Router,
    private libraryS: LibraryService,
    private ratingS: RatingService,
    private fireS: AngularFirestore,
    private userS: UsersService
  ) {}

  ngOnInit(): void {
    this.ar.paramMap
      .pipe(
        map((el) => {
          this.bookId = el.get('id');
          this.bookS.getBookById(el.get('id')).subscribe((el) => {
            this.bookInfoContainer = el;
            this.bookInfoContainer.author === localStorage.getItem('username')
              ? (this.bookAuthor = true)
              : this.bookAuthor;
          });
          this.ratingS
            .getRatingDocumentByBookId(this.bookId)
            .pipe(
              switchMap(async (value) =>
                this.fireS
                  .collection('ratings')
                  .doc(value[0].docId)
                  .valueChanges()
                  .pipe(
                    map((value: any) => {
                      this.rating = value.currentRating;
                      if (value.raters.includes(localStorage.getItem('email')))
                        this.notRated = false;
                    })
                  )
                  .subscribe((value) => {
                    if (value == null || value == undefined) {
                      console.log('boshhhhhh');
                    } else {
                      console.log('jobosh');
                    }
                  })
              )
            )
            .subscribe();
          this.userS
            .getCertainUser(localStorage.getItem('email')!)
            .pipe(
              map((value) => {
                if (value[0].library.includes(this.bookId))
                  this.inLibrary = true;
              })
            )
            .subscribe();
        })
      )
      .subscribe();
  }

  readBook() {
    console.log(this.bookId);
    this.router.navigate(['/book/', this.bookId, 'read']);
  }

  addToLibrary(bookId: string) {
    this.libraryS.addBookToLibrary(bookId, localStorage.getItem('email'));
    this.inLibrary = true;
  }

  openRatingModal() {
    this.displayModal.next(true);
  }

  removeFromLibrary(bookId: string) {
    this.libraryS.removeBookFromLibrary(bookId, localStorage.getItem('email'));
    this.inLibrary = false;
  }
}
