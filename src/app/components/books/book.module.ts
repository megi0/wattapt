import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookPresentationComponent } from './book-presentation/book-presentation.component';
import { BookInnerViewComponent } from './book-inner-view/book-inner-view.component';
import { BookOuterViewComponent } from './book-outer-view/book-outer-view.component';
import { CreateNewBookComponent } from './create-new-book/create-new-book.component';
import { BookListViewComponent } from './book-list-view/book-list-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { UploaderModule } from '../file-uploader/uploader.module';
import { EditBookComponent } from './edit-book/edit-book.component';
import { ModalsModule } from '../modals/modals.module';
import { ChapterToWriteComponent } from './chapter-to-write/chapter-to-write.component';
import { ChapterListerComponent } from './chapter-lister/chapter-lister.component';
import { YourStoriesViewComponent } from './your-stories-view/your-stories-view.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

@NgModule({
  declarations: [
    BookInnerViewComponent,
    BookListViewComponent,
    BookOuterViewComponent,
    BookPresentationComponent,
    CreateNewBookComponent,
    EditBookComponent,
    ChapterToWriteComponent,
    ChapterListerComponent,
    YourStoriesViewComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FroalaEditorModule,
    FroalaViewModule,
    UploaderModule,
    ModalsModule,
    SlickCarouselModule,
    NgxUsefulSwiperModule,
  ],
  exports: [
    BookPresentationComponent,
    BookInnerViewComponent,
    BookOuterViewComponent,
    CreateNewBookComponent,
    BookListViewComponent,
    EditBookComponent,
  ],
  providers: [],
})
export class BookModule {}
