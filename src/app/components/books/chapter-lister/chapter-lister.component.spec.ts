import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterListerComponent } from './chapter-lister.component';

describe('ChapterListerComponent', () => {
  let component: ChapterListerComponent;
  let fixture: ComponentFixture<ChapterListerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChapterListerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterListerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
