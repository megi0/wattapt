import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-chapter-lister',
  templateUrl: './chapter-lister.component.html',
  styleUrls: ['./chapter-lister.component.css'],
})
export class ChapterListerComponent implements OnInit {
  @Input() chapterList: any;
  @Output() chapterNumber = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {
    console.log(this.chapterList);
  }

  pickChapter(chapterNo: number) {
    this.chapterNumber.emit(chapterNo);
  }
}
