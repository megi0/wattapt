import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterToWriteComponent } from './chapter-to-write.component';

describe('ChapterToWriteComponent', () => {
  let component: ChapterToWriteComponent;
  let fixture: ComponentFixture<ChapterToWriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChapterToWriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterToWriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
