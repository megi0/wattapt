import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { BooksService } from 'src/app/_services/books.service';

@Component({
  selector: 'app-chapter-to-write',
  templateUrl: './chapter-to-write.component.html',
  styleUrls: ['./chapter-to-write.component.css'],
})
export class ChapterToWriteComponent implements OnInit {
  chapterContainer: any;
  currentChapterNumber: number = 0;
  currentChapterContent: any;
  currentChapterTitle: any;
  bookId: string | any;

  constructor(private bookS: BooksService, private ar: ActivatedRoute) {}

  ngOnInit(): void {
    this.ar.paramMap
      .pipe(
        map((el) => {
          this.bookId = el.get('id');
          this.bookS
            .getBookChapters(el.get('id'))
            .pipe(
              map((val) => {
                this.chapterContainer = val;
                localStorage.setItem(
                  'book',
                  JSON.stringify(this.chapterContainer[0])
                );
                this.currentChapterContent =
                  this.chapterContainer[0].chapters[
                    this.currentChapterNumber
                  ].content;
                this.currentChapterTitle =
                  this.chapterContainer[0].chapters[
                    this.currentChapterNumber
                  ].title;
              })
            )
            .subscribe();
        })
      )
      .subscribe();
  }

  changeChapter(chapterNo: number) {
    this.currentChapterNumber = chapterNo;
    this.currentChapterContent =
      this.chapterContainer[0].chapters[chapterNo].content;
    console.log('current chap content ', this.currentChapterContent);
    this.currentChapterTitle =
      this.chapterContainer[0].chapters[chapterNo].title;
  }

  addChapter(bookId: string) {
    this.bookS.addChapterToExistingBook(bookId);
    this.currentChapterNumber++;
  }

  saveChapter(bookId: string, chapterNo: number) {
    console.log(chapterNo);
    let book = JSON.parse(localStorage.getItem('book')!);
    console.log('libri ', this.currentChapterTitle);
    book.chapters[chapterNo].title = this.currentChapterTitle;
    book.chapters[chapterNo].content = this.currentChapterContent;
    localStorage.setItem('book', JSON.stringify(book));
    this.bookS.updateBook(bookId, book);
  }
}
