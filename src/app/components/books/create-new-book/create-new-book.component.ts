import { formatCurrency } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable, of, Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { BooksService } from 'src/app/_services/books.service';
import { RatingService } from 'src/app/_services/rating.service';
import { UsersService } from 'src/app/_services/users.service';
import { FileType } from 'src/app/_validators/fileType.validator';

@Component({
  selector: 'app-create-new-book',
  templateUrl: './create-new-book.component.html',
  styleUrls: ['./create-new-book.component.css'],
})
export class CreateNewBookComponent implements OnInit {
  previewURL: string = '../../../../assets/cover-images/no-cover.png';
  createBookForm = this.fb.group({
    title: ['', Validators.required],
    genre: ['', Validators.required],
    description: [''],
    cover: ['', [FileType.image, Validators.required]],
  });
  storageFileName: string = '';
  uploadFile: Subject<any> = new Subject();
  uploadCompleted: Subject<any> = new Subject();
  username: string | any;
  bookId: string | any;

  constructor(
    private fb: FormBuilder,
    private fireS: AngularFirestore,
    private userS: UsersService,
    private bookS: BooksService,
    private ratingS: RatingService
  ) {}

  ngOnInit(): void {}

  showButtonClicked() {
    this.uploadFile.next(true);
  }

  uploadBookInfo(coverPath: string) {
    this.userS
      .getCertainUser(localStorage.getItem('email')!)
      .pipe(
        take(1),
        map((value) => {
          let bookRef = this.fireS.collection('books').doc();
          console.log('pathi i fotos', coverPath);
          bookRef.set({
            title: this.createBookForm.get('title')?.value,
            genre: this.createBookForm.get('genre')?.value,
            description: this.createBookForm.get('description')?.value,
            cover: coverPath,
            author: value[0].username,
          });
          this.createNewChapter(bookRef.ref.id);
          this.createRatingEntry(bookRef.ref.id);
          this.uploadCompleted.next(true);
          this.createBookForm.reset();
        })
      )
      .subscribe();
  }

  assignPreviewImageURL(previewImageURL: string) {
    this.previewURL = previewImageURL;
  }

  createNewChapter(bookId: string) {
    this.bookS.addChapterToNewBook(bookId);
  }

  createRatingEntry(bookId: string) {
    this.ratingS.createRatingEntry(bookId);
    console.log(0 / 0);
  }
}
