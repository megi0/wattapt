import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourStoriesViewComponent } from './your-stories-view.component';

describe('YourStoriesViewComponent', () => {
  let component: YourStoriesViewComponent;
  let fixture: ComponentFixture<YourStoriesViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YourStoriesViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YourStoriesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
