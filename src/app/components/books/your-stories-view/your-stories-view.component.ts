import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { BooksService } from 'src/app/_services/books.service';
import { UsersService } from 'src/app/_services/users.service';
import { Book } from 'src/app/_models/book.model';

@Component({
  selector: 'app-your-stories-view',
  templateUrl: './your-stories-view.component.html',
  styleUrls: ['./your-stories-view.component.css'],
})
export class YourStoriesViewComponent implements OnInit {
  myBooks: Book[] | any;
  myBookProperties: any;
  constructor(
    private userS: UsersService,
    private bookS: BooksService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userS
      .getCertainUser(localStorage.getItem('email')!)
      .pipe(
        map((value) => {
          this.bookS
            .getBookByProperty(value[0].username, 'author')
            .subscribe((element) => {
              this.myBooks = element;
              console.log(this.myBooks);
            });
        })
      )
      .subscribe();
  }
}
