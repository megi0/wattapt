import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploaderComponent,
      multi: true,
    },
  ],
})
export class FileUploaderComponent implements OnInit, ControlValueAccessor {
  private imageFileTypes = [
    'image/apng',
    'image/bmp',
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
  ];
  title = 'cloudsSorage';
  fb: any;
  downloadURL: Observable<string> | any;
  reader = new FileReader();
  storageFileName: string = '';
  onChange!: Function;
  private file: Blob | null = null;
  profileUrl: Observable<string | null> | any;
  @Output() fileEvent = new EventEmitter<any>();
  @Output() filePath = new EventEmitter<string | any>();
  @Input() uploadFile: Observable<any> = new Observable();
  uploadFile$: Subscription = new Subscription();
  urlForDummies = '';
  fileLoading: boolean = false;

  @HostListener('change', ['$event.target.files']) emitFiles(event: FileList) {
    const file = event && event.item(0);
    this.onChange(file);
    file != null
      ? (this.storageFileName = this.createFileName(file.name))
      : this.storageFileName;
    this.file = file;
    if (this.file != null) {
      this.reader.readAsDataURL(this.file);
      this.reader.onload = (_event) => {
        if (this.typeChecker(file?.type) == true)
          this.fileEvent.emit(this.reader.result);
      };
    }
  }

  constructor(
    private host: ElementRef<HTMLInputElement>,
    private storage: AngularFireStorage
  ) {}

  writeValue(obj: any): void {
    this.host.nativeElement.value = '';
    this.file = null;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    console;
  }

  registerOnTouched(fn: any): void {
    //throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    this.uploadFile$ = this.uploadFile
      .pipe(
        map((value) => {
          if (value == true) this.onFileSelected(this.file);
        })
      )
      .subscribe();
  }

  onFileSelected(givenFile: any) {
    const file = givenFile;
    const filePath = `covers/${this.storageFileName}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`covers/${this.storageFileName}`, file);
    this.fileLoading = true;
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe((url: any) => {
            if (url) {
              this.fb = url;
              this.filePath.emit(this.fb);
              this.fileLoading = false;
            }
          });
        })
      )
      .subscribe();
  }

  createFileName(name: string): string {
    return Date.now() + name;
  }

  typeChecker(type: string | any): boolean {
    return this.imageFileTypes.includes(type);
  }
}
