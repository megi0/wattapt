import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibraryComponent } from './library/library.component';
import { BookModule } from '../books/book.module';

@NgModule({
  declarations: [LibraryComponent],
  imports: [CommonModule, BookModule],
  exports: [LibraryComponent],
})
export class LibraryModule {}
