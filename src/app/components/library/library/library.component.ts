import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { BooksService } from 'src/app/_services/books.service';
import { LibraryService } from 'src/app/_services/library.service';
import { UsersService } from 'src/app/_services/users.service';
import { Book } from 'src/app/_models/book.model';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css'],
})
export class LibraryComponent implements OnInit {
  libraryBooksIds: any;
  libraryBooks: [] | any = [];
  bookProperties: any;
  getBooksSubscription: Subscription = new Subscription();
  getUserIdSubscription: Subscription = new Subscription();
  receivedLibraryBooksResponse: boolean = false;

  constructor(
    private libraryS: LibraryService,
    private userS: UsersService,
    private bookS: BooksService
  ) {}

  ngOnInit(): void {
    this.getUserIdSubscription = this.userS
      .getCertainUser(localStorage.getItem('email')!)
      .pipe(
        take(1),
        map((value) => {
          this.receivedLibraryBooksResponse = true;
          this.libraryBooksIds = value[0].library;
          this.libraryBooksIds.forEach((element: any) => {
            this.getBooksSubscription = this.bookS
              .getBookById(element)
              .pipe(
                take(1),
                map((val) => {
                  this.bookProperties = { ...val, key: element };
                  this.libraryBooks = [
                    this.bookProperties,
                    ...this.libraryBooks,
                  ];
                })
              )
              .subscribe();
          });
        })
      )
      .subscribe((val) => {});
  }

  ngOnDestroy() {
    this.getBooksSubscription.unsubscribe();
    this.getUserIdSubscription.unsubscribe();
  }
}
