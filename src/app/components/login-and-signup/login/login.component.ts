import { Component, NgZone, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { UsersService } from 'src/app/_services/users.service';
import { User } from 'src/app/_models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  users: User[] | any;
  correctCredentials: boolean = true;
  userState: any;
  ids: Object[] | any;

  loginForm = this.fb.group({
    email: [''],
    password: [''],
  });

  constructor(
    private fb: FormBuilder,
    private us: UsersService,
    private as: AuthenticationService,
    private rt: Router,
    public afAuth: AngularFireAuth,
    private fs: AngularFirestore,
    public ngZone: NgZone
  ) {
    this.us.getIDs();
  }

  ngOnInit(): void {}

  onClick(email: string, password: string) {
    this.correctCredentials = true;
    this.as.getAccountInfo(email, password).subscribe((el: User[]) => {
      if (el.length == 0) {
        this.correctCredentials = false;
      } else {
        this.as.setLoggedIn(true);
        localStorage.setItem('email', el[0].email);
        localStorage.setItem('username', el[0].username);
        if (el[0].role === 'admin') {
          localStorage.setItem('role', 'admin');
          this.as.setAdmin(true);
        } else {
          localStorage.setItem('role', 'user');
        }
        this.rt.navigate(['/home']);
      }
    });
  }
}
