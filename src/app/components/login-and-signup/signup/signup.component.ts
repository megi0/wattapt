import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/_models/user.model';
import { AgeValidator } from '../../../_validators/ageValidator.validator';
import { RegexpValidator } from 'src/app/_validators/regexpValidator.validator';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  users: User[] | any;
  usernameUnique: boolean = true;
  emailUnique: boolean = true;
  addAdminCompleted: Subject<any> = new Subject();
  @Input() rolePicker: string = 'user';
  signupForm = this.fb.group({
    username: [
      '',
      [Validators.required, RegexpValidator.usernamePatternValidator],
    ],
    email: ['', [Validators.required, RegexpValidator.emailPatternValidator]],
    password: ['', [Validators.required, Validators.minLength(8)]],
    birthday: ['', [Validators.required, AgeValidator.propriateAgeValidator]],
  });

  constructor(
    private fb: FormBuilder,
    private fs: AngularFirestore,
    private rt: Router,
    private as: AuthenticationService
  ) {}

  ngOnInit(): void {
    console.log(this.rolePicker);
  }

  onClick(email: string, username: string) {
    this.usernameUnique = true;
    this.emailUnique = true;
    let emailSubscription = this.as
      .getAccountWithGivenEmail(email)
      .subscribe((el: User[]) => {
        if (el.length != 0) {
          this.emailUnique = false;
        }
        emailSubscription.unsubscribe();
        let usernameSubscription = this.as
          .getAccountWithGivenUsername(username)
          .subscribe((el: User[]) => {
            if (el.length != 0) {
              this.usernameUnique = false;
            } else {
              if (this.usernameUnique && this.emailUnique) {
                usernameSubscription.unsubscribe();
                this.signUpAccordingToRoles();
              }
            }
          });
      });
  }

  signUpAccordingToRoles() {
    this.sendUserInfoTodatabase(this.signupForm);
    if (this.rolePicker === 'user') {
      localStorage.setItem('email', this.signupForm.get('email')?.value);
      localStorage.setItem('username', this.signupForm.get('username')?.value);
      this.as.setLoggedIn(true);
      this.rt.navigate(['/home']);
    } else {
      this.addAdminCompleted.next(true);
    }
  }

  sendUserInfoTodatabase(form: FormGroup) {
    this.fs.collection('users').add({
      username: form.get('username')?.value,
      email: form.get('email')?.value,
      birthday: form.get('birthday')?.value,
      password: form.get('password')?.value,
      role: this.rolePicker,
      library: [],
    });
  }
}
