import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css'],
})
export class ConfirmationModalComponent implements OnInit {
  @Input() display: Observable<any> = new Observable();
  @Input() message: string = '';
  @Output() proceedWithInitialAction = new EventEmitter<boolean>();
  shouldDisplay: boolean = false;

  constructor() {}

  ngOnInit(): void {
    this.display
      .pipe(map((value: boolean) => (this.shouldDisplay = value)))
      .subscribe();
  }

  carryOnWithTheAction() {
    console.log('FIRST');
    this.proceedWithInitialAction.emit(true);
    console.log('SECOND');
    this.shouldDisplay = false;
    console.log('THIRD');
  }

  openModal(open: boolean): void {
    this.shouldDisplay = open;
  }
}
