import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskCompletedModalComponent } from './task-completed-modal/task-completed-modal.component';
import { RatingModalComponent } from './rating-modal/rating-modal.component';
import { FormsModule } from '@angular/forms';
import { RatingModule } from 'ng-starrating';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';

@NgModule({
  declarations: [
    TaskCompletedModalComponent,
    RatingModalComponent,
    ConfirmationModalComponent,
  ],
  imports: [CommonModule, FormsModule, RatingModule],
  exports: [
    TaskCompletedModalComponent,
    RatingModalComponent,
    ConfirmationModalComponent,
  ],
})
export class ModalsModule {}
