import { Component, Input, OnInit } from '@angular/core';
import { StarRatingComponent } from 'ng-starrating';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RatingService } from 'src/app/_services/rating.service';

@Component({
  selector: 'app-rating-modal',
  templateUrl: './rating-modal.component.html',
  styleUrls: ['./rating-modal.component.css'],
})
export class RatingModalComponent implements OnInit {
  @Input() bookTitle: string = '';
  @Input() bookId: string = '';
  @Input() display: Observable<any> = new Observable();
  shouldDisplay: boolean = false;
  starRating = 0;
  value: number | any;

  constructor(private ratingS: RatingService) {}

  ngOnInit(): void {
    this.display
      .pipe(map((value: boolean) => (this.shouldDisplay = value)))
      .subscribe();
  }

  onRate($event: {
    oldValue: number;
    newValue: number;
    starRating: StarRatingComponent;
  }) {
    this.value = $event.newValue;
  }

  openModal(open: boolean): void {
    this.shouldDisplay = open;
  }

  submitRating() {
    this.ratingS.rate(this.bookId, localStorage.getItem('email')!, this.value);
    this.shouldDisplay = false;
  }

  ngOnChanges() {}
}
