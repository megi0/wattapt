import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskCompletedModalComponent } from './task-completed-modal.component';

describe('TaskCompletedModalComponent', () => {
  let component: TaskCompletedModalComponent;
  let fixture: ComponentFixture<TaskCompletedModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskCompletedModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskCompletedModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
