import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-task-completed-modal',
  templateUrl: './task-completed-modal.component.html',
  styleUrls: ['./task-completed-modal.component.css'],
})
export class TaskCompletedModalComponent implements OnInit {
  isModalOpen: boolean = false;
  @Input() uploadCompleted$: Observable<any> = new Observable();
  @Input() message: string = '';
  uploadCompletedSub: Subscription | any;

  constructor() {}

  ngOnInit(): void {
    this.uploadCompletedSub = this.uploadCompleted$
      .pipe(
        map((value) => {
          if (value == true) this.openModal(true);
        })
      )
      .subscribe();
  }

  openModal(open: boolean): void {
    this.isModalOpen = open;
  }
}
