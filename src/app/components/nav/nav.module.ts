import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [NavComponent],
  imports: [CommonModule, Ng2SearchPipeModule, RouterModule, FormsModule],
  exports: [NavComponent],
})
export class NavModule {}
