import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { BooksService } from 'src/app/_services/books.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {
  data: string = '';
  books: [] | any;
  constructor(
    public authS: AuthenticationService,
    private router: Router,
    private bookS: BooksService
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem('email') != null) {
      this.authS.setLoggedIn(true);
    }
  }

  goToCreateNewBook() {
    this.router.navigate(['/new-book']);
  }

  goToStories() {
    this.router.navigate(['/my-stories']);
  }

  logOut() {
    localStorage.removeItem('email');
    localStorage.removeItem('username');
    this.router.navigate(['./login']);
  }

  getSearchResults() {
    this.bookS
      .getAllBooks1()
      .pipe(
        take(1),
        map((value) => {
          this.books = value;
        })
      )
      .subscribe();
  }

  navigateToBookPresentation(bookId: string) {
    this.router.navigate(['/book/', bookId]);
    this.books = [];
  }
}
