// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAma0jIH1dLE9rwyTp_Kxv7EZQElgFMAX0',
    authDomain: 'wattapt-1c0ed.firebaseapp.com',
    projectId: 'wattapt-1c0ed',
    storageBucket: 'wattapt-1c0ed.appspot.com',
    messagingSenderId: '64804154891',
    appId: '1:64804154891:web:b2a70df21caefbfe59a10e',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
